package com.example.zipkin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class ApplicationController {

	private Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	RestTemplate restTemplate;

	@GetMapping(value = "/zipkinrest")
	public String zipkinService1() {
		log.info("Inside zipkinrest application controller..");

		restTemplate
				.exchange("http://localhost:8083/sendMessage", HttpMethod.GET, null, new ParameterizedTypeReference<String>() {
				}).getBody();
		return "SuccessFull";
	}
}
